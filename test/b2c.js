const puppeteer = require('puppeteer');
const expect = require('chai').expect;

const {shouldNotExist,shouldExist} = require('../lib/helper')

describe('[B2C-ENUS]GA View Check',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            headless:false,//有無需要開視窗,false要開,true不開
            //slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    // beforeEach(async function(){
    //     browser=await puppeteer.launch({
    //         executablePath:
    //         "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
    //         headless:false,//有無需要開視窗,false要開,true不開
    //         slowMo:100,// slow down by 100ms
    //         devtools:false//有無需要開啟開發人員工具
    //     })
    //     page=await browser.newPage()
    //     await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    //     await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    // })
    // afterEach(async function(){
    //     // Runs after each test steps(所有test step執行完的步驟)
    //     await browser.close()
    // })
    it('B2B URL',async function(){
        await page.setViewport({width:1200,height:1000})
        //B2C上面的Business是否為該國家的B2B網站
        await page.goto('https://www.benq.com/en-us/index.html')
        var enusB2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
        var enusB2bURL = await page.$eval(enusB2bURLSelector, element=> element.getAttribute("href"))
        console.log("EN-US B2B URL:",enusB2bURL)
        expect(enusB2bURL).to.include('en-us')
    })
    it('B2B club-lang',async function(){
        await page.setViewport({width:1200,height:1000})
        //EN-US有club-lang會員中心
        await page.goto('https://www.benq.com/en-us/index.html')
        await page.waitForTimeout(5000)//等待5000毫秒
        var signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area'
        await shouldExist(page,signInSelector)
        await page.click('body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area > a')
        await page.waitForTimeout(10000)//等待10000毫秒
        await page.waitForSelector('#AccountLogin')
        const b2bClublangurl = await page.url()
        console.log("EN-US club-lang URL:",b2bClublangurl)
        expect(b2bClublangurl).to.include('club.benq.com')
        expect(b2bClublangurl).to.include('lang=en-us')
    })
    it('EC URL',async function(){
        await page.setViewport({width:1200,height:1000})
        //EC URL
        await page.goto('https://www.benq.com/en-us/lamps/desklamp/desklamp-genie.html')
        await page.waitForTimeout(10000)//等待10000毫秒
        var ecUrlSelector = 'body > section.component-container-products.phase3-component-container-products > div > div > div:nth-child(2) > div > div.main_buy > a'
        var ecUrl = await page.$eval(ecUrlSelector, element=> element.getAttribute("href"))
        console.log("EN-US EC URL:",ecUrl)
        expect(ecUrl).to.include('us-buy')
    })
})

describe('[B2C-ENAP]GA View Check',()=>{
    let browser
    let page
    //Test Hooks:before, beforeEach, after, afterEach
    //before:每個test case執行之前先做的動作(page/browswer)
    before(async function(){
        browser=await puppeteer.launch({
            executablePath:
            "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            headless:false,//有無需要開視窗,false要開,true不開
            //slowMo:100,// slow down by 100ms
            devtools:false//有無需要開啟開發人員工具
        })
        page=await browser.newPage()
        await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
        await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    })
    //after:每個test case執行之後統一要做的動作(page/browswer)
    after(async function(){
        await browser.close()
    })
    // beforeEach(async function(){
    //     browser=await puppeteer.launch({
    //         executablePath:
    //         "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
    //         headless:false,//有無需要開視窗,false要開,true不開
    //         slowMo:100,// slow down by 100ms
    //         devtools:false//有無需要開啟開發人員工具
    //     })
    //     page=await browser.newPage()
    //     await page.setDefaultTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation, page.waitForFunction, page.waitForFileChooser,page.waitForSelector等method的時間，預設是 30 秒
    //     await page.setDefaultNavigationTimeout(200000)//會修改goto,goBack,goForward,reload, setContent, waitForNavigation等method的時間，預設是 30 秒
    // })
    // afterEach(async function(){
    //     // Runs after each test steps(所有test step執行完的步驟)
    //     await browser.close()
    // })
    it('B2B URL',async function(){
        await page.setViewport({width:1200,height:1000})
        //B2C上面的Business是否為該國家的B2B網站
        await page.goto('https://www.benq.com/en-ap/index.html')
        var enapB2bURLSelector = "body > header > aside.line-1 > aside > nav > div.left-wrapper > a:nth-child(1)"
        var enapB2bURL = await page.$eval(enapB2bURLSelector, element=> element.getAttribute("href"))
        console.log("EN-AP B2B URL:",enapB2bURL)
        expect(enapB2bURL).to.include('en-ap')
    })
    it('B2B club-lang',async function(){
        await page.setViewport({width:1200,height:1000})
        //EN-AP沒有club-lang會員中心
        await page.goto('https://www.benq.com/en-ap/index.html')
        await page.waitForTimeout(5000)//等待5000毫秒
        var signInSelector = 'body > header > aside.line-1 > aside > nav > div.right-wrapper > div.log_area'
        await shouldNotExist(page,signInSelector)
    })
})
